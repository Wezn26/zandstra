<?php 
class StaticExample
{
  static public $num = 0;
  public static function sayHello()
  {
     self::$num++;
     print "Привет! (". self::$num .")\n";
  }
}

StaticExample::sayHello();
var_dump(StaticExample::$num);